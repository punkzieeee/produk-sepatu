<?php

namespace App\Http\Controllers;

use App\Models\Shoe;
use Exception;
use Illuminate\Http\Request;

class ShoeController extends Controller
{
    function listShoes()
    {
        try {
            $data = Shoe::all();
            return response()->json([
                'message' => 'Success',
                'data' => $data
            ]);
        } catch (Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        
    }

    function searchShoes($id)
    {
        try {
            $data = Shoe::find($id);
            return response()->json([
                'message' => 'Success',
                'data' => $data
            ]);
        } catch (\Throwable $th) {
            return response()->json(['message'=>'Failed']);
        }
        
    }

    function insertShoes(Request $request)
    {
        try {
            $request->validate([
                'nama_produk' => 'required',
                'brand' => 'required',
                'deskripsi' => 'required',
                'harga' => 'required'
            ]);
            
            $data = new Shoe();
            $data -> nama_produk = $request->nama_produk;
            $data -> brand = $request->brand;
            $data -> deskripsi = $request->deskripsi;
            $data -> harga = $request->harga;
            $data -> save();

            return response()->json(['message'=>'Success']);
        } catch (Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
    }

    function updateShoes($id, Request $request)
    {
        try {
            $request->validate([
                'nama_produk' => 'required',
                'brand' => 'required',
                'deskripsi' => 'required',
                'harga' => 'required'
            ]);
            
            $data = Shoe::find($id);
            $data -> nama_produk = $request->nama_produk;
            $data -> brand = $request->brand;
            $data -> deskripsi = $request->deskripsi;
            $data -> harga = $request->harga;
            $data -> save();

            return response()->json(['message'=>'Success']);
        } catch (Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
    }

    function deleteShoes($id)
    {
        try {
            // $data = Shoe::where('id', '=', $id);
            $data = Shoe::find($id);
            $data -> delete();
            return response()->json($data);
        } catch (\Throwable $th) {
            return response()->json(['message'=>'Failed']);
        }  
    }
}
