<?php

namespace Database\Seeders;

use App\Models\Shoe;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ShoeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shoe::insert([[
            'nama_produk' => 'Converse x Virgil Abloh',
            'brand' => 'Converse',
            'deskripsi' => 'Quality Import Premium',
            'harga' => 530000
        ], [
            'nama_produk' => 'Slip On Classic Checkerboard Original',
            'brand' => 'Vans',
            'deskripsi' => 'Brand New In Box (BNIB)',
            'harga' => 1006900
        ], [
            'nama_produk' => 'New Kreate Chukka',
            'brand' => 'League',
            'deskripsi' => 'Brand New In Box (BNIB)',
            'harga' => 799000
        ], [
            'nama_produk' => 'Instalite Pro Hthr Navy',
            'brand' => 'Reebok',
            'deskripsi' => 'Original',
            'harga' => 785000
        ], [
            'nama_produk' => 'Suede Ignite',
            'brand' => 'Puma',
            'deskripsi' => 'Brand New In Box (BNIB)',
            'harga' => 1399000
        ]]);
    }
}
